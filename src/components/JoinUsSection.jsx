import React, { useState } from "react";
import axios from "axios";

const JoinUsSection = () => {
  const [email, setEmail] = useState("");
  const [subscribed, setSubscribed] = useState(false);
  const [unsubscribed, setUnsubscribed] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleSubscribe = (event) => {
    event.preventDefault();
    setIsSubmitting(true);
    axios
      .post("http://localhost:3000/subscribe", { email })
      .then((response) => {
        setSubscribed(true);
        setIsSubmitting(false);
        console.log(response)
      })
      .catch((error) => {
        if (error.response && error.response.status === 422) {
          window.alert(error.response.data.error);
        } else {
          console.error(error);
        }
        setIsSubmitting(false);
      });
  };

  const handleUnsubscribe = (event) => {
    event.preventDefault();
    setIsSubmitting(true);
    axios
      .post("http://localhost:3000/unsubscribe", { email })
      .then((response) => {
        console.log("Unsubscribed")
        setUnsubscribed(true);
        setIsSubmitting(false);
        console.log(response)
      })
      .catch((error) => {
        console.error(error);
        setIsSubmitting(false);
      });
  };

  return (
    <section className="app-section app-section--join-our-program">
      <h2 className="app-title">Join Our Program</h2>

      <h3 className="app-subtitle app-subtitle--text-gray">
        Sed do eiusmod tempor incididunt <br />
        ut labore et dolore magna aliqua.
      </h3>

      {!subscribed && !unsubscribed && (
        <form className="app-section__form" onSubmit={handleSubscribe}>
          <input
            type="email"
            className="app-section__input"
            placeholder="Email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          ></input>
          <button
            type="submit"
            className="app-section__button app-section__button--subscribe"
            style={{ opacity: isSubmitting ? 0.5 : 1 }}
            disabled={isSubmitting}
          >
            SUBSCRIBE
          </button>
        </form>
      )}
      {subscribed && (
        <form onSubmit={e => handleUnsubscribe(e)}>
          <button
            type="submit"
            className="app-section__button app-section__button--subscribe"
            style={{ opacity: isSubmitting ? 0.5 : 1 }}
            disabled={isSubmitting}
          >
            UNSUBSCRIBE
          </button>
        </form>
      )}

    </section>
  );
};

export default JoinUsSection;
