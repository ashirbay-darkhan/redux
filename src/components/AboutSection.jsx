import React from 'react';

const AboutSection = () => {
  return (
    <section className="app-section app-section--image-culture">
      <h2 className="app-title">
        Learn more<br/> about our culture...
      </h2>

      <button className="app-section__button app-section__button--our-culture">
        &#9654;
      </button>

      <h3 className="app-subtitle">
        Duis aute irure dolor in <br/>reprehenderit in voluptate velit esse
        <br/> cillum dolore eu fugiat nulla
      </h3>
    </section>
  );
};

export default AboutSection;
