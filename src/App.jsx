import './App.css'
import HeaderSection from "./components/HeaderSection.jsx";
import CommunitySection from "./components/CommunitySection.jsx";
import AboutSection from "./components/AboutSection.jsx";
import JoinUsSection from "./components/JoinUsSection.jsx";
import FooterSection from "./components/FooterSection.jsx";

function App() {

  return (
    <>
      <main id="app-container">
        <HeaderSection />
        <CommunitySection />
        <AboutSection />
        <JoinUsSection />
        <FooterSection />
      </main>
    </>
  )
}

export default App
