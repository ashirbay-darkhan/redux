import React from 'react';
import logoHeader from '../assets/images/your-logo-here.png'

const HeaderSection = () => {
  return (
    <>
      <header className="app-header">
        <a className="app-header__logo" href="#">Project</a>

        <nav className="app-header__nav">
          <ul className="app-header__nav-list">
            <li className="app-header__nav-list-item"><a href="#">Projects</a></li>
            <li className="app-header__nav-list-item"><a href="#">About us</a></li>
            <li className="app-header__nav-list-item"><a href="#">Stories</a></li>
            <li className="app-header__nav-list-item"><a href="#">Contact</a></li>
          </ul>
        </nav>

        <div className="app-header__nav-menu-button">
          <div className="app-header__nav-menu-button-icon">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      </header>

      <section className="app-section app-section--image-overlay app-section--image-peak">
        <img src={logoHeader} alt="Logo icon picture" className="app-logo"/>
        <h1 className="app-title">
          Your Headline <br/> Here
        </h1>
        <h2 className="app-subtitle">
          Lorem ipsum dolor sit amet, consectetur <br/>adipiscing elit sed do eiusmod.
        </h2>
      </section>
    </>
  );
};

export default HeaderSection;
