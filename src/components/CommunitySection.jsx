import React, { useEffect, useState } from "react";
import axios from "axios";

const CommunitySection = () => {
  const [communityData, setCommunityData] = useState([]);
  const [visible, setVisible] = useState(true);

  const toggleVisibility = () => {
    setVisible(!visible);
  };

  useEffect(() => {
    axios
      .get("http://localhost:3000/community")
      .then((response) => {
        setCommunityData(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  return (
    <section className="app-section">
      <h2 className="app-title">
        Big Community of
        <br /> People Like You
      </h2>
      <button className="hide-toggle-btn" onClick={toggleVisibility}>
        {visible ? "Hide section" : "Show section"}
      </button>
      {visible && (
        <>
          <h3 className="app-subtitle">
            We're proud of our products, and we're really excited
            <br /> when we get feedback from our users.
          </h3>
          <div className="users-card">
            {communityData.map((community) => (
              <div className="card" key={community.id}>
                <img
                  src={community.avatar}
                  alt="user-image"
                  className="card__avatar"
                ></img>
                <p className="card__description">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Assumenda, quod.
                </p>
                <p className="card__name">
                  {community.firstName} {community.lastName}
                </p>
                <p className="card__position">{community.position}</p>
              </div>
            ))}
          </div>
        </>
      )}
    </section>
  );
};

export default CommunitySection;
