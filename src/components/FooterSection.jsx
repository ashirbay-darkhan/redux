import React from 'react';
import logoFooter from '../assets/images/your-logo-footer.png'

const FooterSection = () => {
  return (
    <footer className="app-footer">
      <div className="app-footer__logo">
        <img src={logoFooter} alt="Logo small icon picture"
             className="app-logo app-logo--small"/> Project
      </div>

      <address className="app-footer__address">
        123 Street,<br/>
        Anytown, USA 12345
      </address>

      <a className="app-footer__email" href="mailto:hello@website.com">
        hello@website.com
      </a>

      <p className="app-footer__rights">
        &copy; 2021 Project. All rights reserved
      </p>
    </footer>
  );
};

export default FooterSection;
